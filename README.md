# OpenML dataset: Solar-Radiation-Prediction

https://www.openml.org/d/43751

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Space Apps Moscow was held on April 29th  30th. Thank you to the 175 people who joined the International Space Apps Challenge at this location!
Content
The dataset contains such columns as: "wind direction", "wind speed", "humidity" and temperature. The response parameter that is to be predicted is: "Solar_radiation". It contains measurements for the past 4 months and you have to predict the level of solar radiation.
Just imagine that you've got solar energy batteries and you want to know will it  be reasonable to use them in future?
Acknowledgements
Thanks NASA for the dataset.
Inspiration
Predict the level of solar radiation.
Here are some intersecting dependences that i have figured out:

Humidity  Solarradiation.
2.Temeperature  Solarradiation.

The best result of accuracy  I could get using cross-validation was only 55.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43751) of an [OpenML dataset](https://www.openml.org/d/43751). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43751/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43751/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43751/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

